FROM jwilder/nginx-proxy
MAINTAINER contact@4ge.it

# tunning
RUN { \
      echo 'server_tokens off;'; \
      echo 'client_max_body_size 100m;'; \
      echo 'client_body_timeout 5000s;'; \
      echo 'client_header_buffer_size 10m;'; \
      echo 'client_header_timeout 5000s;'; \
      echo 'proxy_read_timeout 1800s;'; \
    } > /etc/nginx/conf.d/my_proxy.conf
